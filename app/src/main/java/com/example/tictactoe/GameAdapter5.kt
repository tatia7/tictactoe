package com.example.tictactoe

class GameAdapter5 {
    private var currentPlayer = 1
    val currentPlayerActive : String
        get() {
            return if (currentPlayer == 1) "X" else "O"
        }
    private var mode = arrayOf(
        intArrayOf(0,0,0,0,0),
        intArrayOf(0,0,0,0,0),
        intArrayOf(0,0,0,0,0),
        intArrayOf(0,0,0,0,0),
        intArrayOf(0,0,0,0,0)
    )
    fun moving(position: playerPosition) : WinningLine?{
        mode[position.row][position.column] = currentPlayer
        val winningLine = gameEnded()
        if (winningLine == null){
            currentPlayer = 5 - currentPlayer
        }
        return winningLine
    }
    private fun gameEnded(): WinningLine?{
        if (mode[0][0] == currentPlayer && mode[0][1] == currentPlayer && mode[0][2] == currentPlayer && mode[0][3] == currentPlayer && mode[0][5] == currentPlayer){
            return WinningLine.ROW_0

        } else if (mode[1][0] == currentPlayer && mode[1][1] == currentPlayer && mode[1][2] == currentPlayer && mode[1][3] == currentPlayer && mode[1][4] == currentPlayer) {
            return WinningLine.ROW_1
        } else if (mode[2][0] == currentPlayer && mode[2][1] == currentPlayer && mode[2][2] == currentPlayer && mode[2][3] == currentPlayer && mode[2][4] == currentPlayer) {
            return WinningLine.ROW_2
        }else if (mode[3][0] == currentPlayer && mode[3][1] == currentPlayer && mode[3][2] == currentPlayer && mode[3][3] == currentPlayer && mode[3][4] == currentPlayer) {
            return WinningLine.ROW_3
        }else if (mode[4][0] == currentPlayer && mode[4][1] == currentPlayer && mode[4][2] == currentPlayer && mode[4][3] == currentPlayer && mode[4][4] == currentPlayer) {
            return WinningLine.ROW_4
        } else if (mode[0][0] == currentPlayer && mode[1][0] == currentPlayer && mode[2][0] == currentPlayer && mode[3][0] == currentPlayer && mode[4][0] == currentPlayer) {
            return WinningLine.COLUMN_0
        } else if (mode[0][1] == currentPlayer && mode[1][1] == currentPlayer && mode[2][1] == currentPlayer&& mode[3][1] == currentPlayer && mode[4][1] == currentPlayer) {
            return WinningLine.COLUMN_1
        } else if (mode[0][2] == currentPlayer && mode[1][2] == currentPlayer && mode[2][2] == currentPlayer&& mode[3][2] == currentPlayer && mode[4][2] == currentPlayer) {
            return WinningLine.COLUMN_2
        }else if (mode[0][3] == currentPlayer && mode[1][3] == currentPlayer && mode[2][3] == currentPlayer&& mode[3][3] == currentPlayer && mode[4][3] == currentPlayer) {
            return WinningLine.COLUMN_3
        }else if (mode[0][4] == currentPlayer && mode[1][4] == currentPlayer && mode[2][4] == currentPlayer&& mode[3][4] == currentPlayer && mode[4][4] == currentPlayer) {
                return WinningLine.COLUMN_2
        } else if (mode[0][0] == currentPlayer && mode[1][1] == currentPlayer && mode[2][2] == currentPlayer) {
            return WinningLine.DIAGONAL_LEFT
        } else if (mode[0][2] == currentPlayer && mode[1][1] == currentPlayer && mode[2][0] == currentPlayer) {
            return WinningLine.DIAGONAL_RIGHT
        }
        return null
    }
}