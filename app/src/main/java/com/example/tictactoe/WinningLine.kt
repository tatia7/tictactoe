package com.example.tictactoe

enum class WinningLine {
    ROW_0,
    ROW_1,
    ROW_2,
    ROW_3,
    ROW_4,
    COLUMN_0,
    COLUMN_1,
    COLUMN_2,
    COLUMN_3,
    COLUMN_4,
    DIAGONAL_LEFT,
    DIAGONAL_RIGHT,
}